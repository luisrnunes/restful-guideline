# Guia para desenvolvimento de serviços RESTful
Abaixo estão descritas as principais regras para padronização do desenvolvimento de serviços RESTful.

#### Terminologia
- **Recurso** e **subrecurso** são representações de alguma entidade ou atributo pertencente ao domínio do seu negócio. As operações do seu serviço poderão manipular um determinado recurso. Por exemplo: será possível buscar, adicionar, atualizar ou excluir um ```Cliente```.

- **Rota** é a URL pela qual será possível realizar operações em um determinado recurso.

- **Payload** é o corpo da requisição ou da resposta do serviço, geralmente no formato JSON.

### Padrões para Rotas
As rotas de um serviço devem seguir alguns padrões para estarem de acordo com a arquitetura RESTful. Abaixo estão descritas principais regras:

###### Recursos no singular ou plural?
Apenas por questões de padronização, os nomes dos recursos devem estar no plural.

###### Não utilize verbos na rota
Uma rota RESTful não deve conter verbos em sua definição. O método HTTP deve ser o responsável por indicar qual o tipo de operação será realizada. Assim sendo, **evite** rotas como:

- /addClient
- /findClient
- /updateClient
- /deleteClient

Em uma arquitetura RESTful, tais rotas devem ser definidas da seguinte forma:

| **Método HTTP** | **Rota** | **Descrição** |
| --- | --- | --- |
| GET | /clients | Busca a coleção de clientes |
| GET | /clients/1 | Busca o cliente com ID 1 |
| POST | /clients | Cria um novo cliente |
| PUT | /clients/1 | Atualiza o cliente com ID 1 |
| DELETE | /clients/1 | Exclui o cliente com ID 1 |

Desta forma, a rota indica o recurso que será manipulado e o método HTTP indica a ação.

###### Rotas para recursos relacionados
Quando houver relacionamento entre recursos, as rotas devem indicar tal estrutura, seguindo o mesmo padrão de definição descrito acima. Exemplos:

| **Método HTTP** | **Rota** | **Descrição** |
| --- | --- | --- |
| GET | /clients/1/addresses | Busca o endereço do cliente com ID 1 |
| POST | /clients/1/addresses | Cria um novo endereço para o cliente com ID 1 |
| PUT | /clients/1/addresses | Atualiza o endereço do cliente com ID 1 |
| DELETE | /clients/1/addresses | Exclui o endereço do cliente com ID 1 |

### Métodos HTTP
Os métodos HTTP, **também conhecidos como verbos HTTP**, devem ser utilizados para indicar a operação que será realizada em um determinado recurso. Abaixo está descrita a relação mais comumente utilizada entre operação e método HTTP:

###### GET e HEAD – Buscando e filtrando recursos
O método GET serve para buscar recursos. A identificação do recurso deverá estar contida na rota. Filtros adicionais devem ser informados via query string. Por exemplo:

- Rota para buscar o recurso Cliente com ID 1:
  - /clients/1
- Rota para buscar Clientes com status &quot;active&quot;:
  - /clients?filter[status]=active

O método HEAD tem a mesma finalidade do GET, porém não deve enviar uma payload de resposta. Utilizar o HEAD é útil quando uma determinada rota serve apenas para verificar se um recurso existe ou não. Neste caso, o resultado deverá ser indicado através dos status HTTP, que estão descritos em outro tópico abaixo.
Os benefícios da utilização do HEAD estão principalmente relacionados ao menor consumo de dados por parte de quem está utilizando o serviço e à maior facilidade de implementação.

###### POST – Criando recursos
O método POST deve ser utilizado para criar novos recursos no servidor. Um POST para a rota ```/clients``` criará um novo Cliente. **Opcionalmente** e a depender de questões de segurança, o serviço poderá retornar o header ```Location``` com a rota para visualizar o recurso criado.

###### PUT – Atualização completa de recursos
O método PUT deve ser utilizado para fazer a atualização de **todos os atributos** de um determinado recurso. Geralmente, a utilização do PUT é precedida por uma requisição GET, que retorna todos os atributos do recurso em questão.  

Para atualização de **um único atributo** do recurso, é possível utilizar uma abordagem em que tal atributo é tratado com um subrecurso. Por exemplo, para atualizar o atributo ```name``` do recurso ```client``` pode ser definida a seguinte rota:

| **Método HTTP** | **Rota** | **Descrição** |
| --- | --- | --- |
| PUT | /clients/1/name | Atualiza o nome do cliente com ID 1 |

Repare que, na rota, o atributo ```name``` foi definido como um subrecurso de ```client```. Esta abordagem está de acordo com os padrões RESTful e pode ser adotada sem problemas.

###### PATCH - Atualização parcial de recursos
Para atualização parcial, ou seja, de dois ou mais atributos de um recurso, deve ser utilizado o método PATCH. Por exemplo, para atualizar os atributos ```name``` e ```email``` do recurso ```client```, pode ser definida a seguinte rota:

| **Método HTTP** | **Rota** | **Descrição** |
| --- | --- | --- |
| PATCH | /clients/1 | Atualiza parcialmente o cliente com ID 1 |

E a seguinte payload de requisição:
```
{
  "attributes": {
    "name": "José da Silva",
    "email": "josedasilva@gmail.com"
  }
}
```

###### DELETE
O método DELETE deve ser utilizado para excluir um recurso, seja a exclusão lógica ou física.

### HTTP Status
Os status HTTP são códigos numéricos que indicam o resultado de uma operação em um serviço RESTful.

##### Status da família 200
Os status da família 200 indicam que a operação foi recebida e interpretada com sucesso. Abaixo as descrições dos status mais utilizados e casos de uso mais comuns:

###### 200 OK
Status de sucesso genérico.

###### 201 Created
Status retornado após a criação de um novo recurso.

###### 202 Accepted
Status retornado quando a operação foi interpretada e aceitada pelo serviço, mas o processamento ocorrerá em outro momento. Comumente utilizado em operações que envolvem enfileiramento de requisições.

###### 204 No Content
Status retornado quando a requisição foi concluída, mas o serviço não enviará uma payload de resposta. Geralmente é utilizado em operações de exclusão e atualização de recursos.

##### Status da família 400
Os status da família 400 indicam que houve algum **erro do usuário**, como envio de dados inválidos, credenciais incorretas, método HTTP incorreto e etc. Abaixo as descrições dos status mais utilizados e casos de uso mais comuns:

###### 400 Bad Request
Status que indica um erro de formação de sintaxe na requisição.

###### 401 Unauthorized
Status retornado quando as credenciais de utilização do serviço estão incorretas.

###### 404 Not Found
Status retornado quando o recurso buscado não existe. Não está limitado apenas aos métodos GET e HEAD. Um PUT, PATCH ou DELETE para um recurso que não existe também deve retornar 404.

###### 405 Method Not Allowed
Status retornado quando, por exemplo, for enviada uma requisição POST para uma rota que espera um GET.

###### 409 Conflict
Comumente retornado quando uma requisição repetida é enviada. Por exemplo: imagine uma estrtura de banco de dados na qual o atributo ```email``` está definido como UNIQUE. Ao receber um POST com um email já cadastrado, o serviço deve retornar 409.

###### 415 Unsupported Media Type
Status retornado quando, por exemplo, o serviço espera receber uma payload em formato JSON e recebe em XML.

###### 422 Unprocessable Entity
Status retornado quando há algum erro de validação nos dados recebidos como parâmetros.

##### Status da família 500
Os status da família 500 indicam que ocorreu algum **erro no servidor** durante o processamento da requisição. São exemplos deste caso erros ao acessar bancos de dados, executar queries, acessar serviços externos e etc. Abaixo as descrições dos status mais utilizados e casos de uso mais comuns:

###### 500 Internal Server Error
Status genérico que indica erro de processamento no servidor.

###### 504 Gateway Timeout
Status retornado quando ocorre timeout em uma conexão com um serviço externo, como banco de dados ou API.

#### Padronização de payloads
As regras abaixo devem ser aplicadas em **todas** as payloads dos serviços desenvolvidos, sejam elas de requisição ou resposta.

1. Nomes dos atributos em snake_case
2. Nomes dos atributos em inglês
3. WIP.
4. WIP.

Além das regras gerais descritas acima, existem regras específicas que dependem do método e/ou status HTTP. Tais regras estão descritas nos subtópicos abaixo:

##### Payload de resposta para GET
As payloads de resposta para uma requisição GET devem retornar o recurso no formato JSON **sem envolver** os dados em um objeto nomeado.

Exemplo de payload correta:
```GET /clients/1```
```
{
  "id": 1,
  "name": "José da Silva",
  "email": "josedasilva@gmail.com",
  "status": "active"
}
```

Exemplo de payload **incorreta**:
```GET /clients/1```
```
{
  "client": {
    "id": 1,
    "name": "José da Silva",
    "email": "josedasilva@gmail.com",
    "status": "active"
  }
}
```

##### Padrões para payloads de erros
As payloads de resposta para erros, sejam eles da família 400 ou 500, devem conter um atributo ```message``` contendo a descrição do erro. Veja abaixo a tabela com as mensagens de erro para cada HTTP status:

| **HTTP Status** | **Mensagem** |
| --- | --- |
| 400 | Request could not be completed. |
| 401 | Invalid credentials. |
| 404 | Resource not found. |
| 405 | Invalid request method. |
| 415 | Only JSON is accepted as media type. |
| 422 | Validation failed. |
| 500 | Internal server error. |

Para o HTTP Status 422, a payload deve conter um array ```errors``` com maiores detalhes sobre os erros de validação. Exemplo:

```
{
  "message": "Validation failed.",
  "errors": [
    {
      "message": "CPF should contain exactly 11 numerical characters."
    },
    {
      "message": "Password should contain at least 8 characters."
    }
  ]
}
```